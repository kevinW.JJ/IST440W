[![IST440W](https://i.imgur.com/J0RQ05U.png)](https://gitlab.com/kevinW.JJ/IST440W/)
# IST440W-Decoder

[![Build status](https://gitlab.com/kevinW.JJ/IST440W/badges/master/build.svg)](https://gitlab.com/kevinW.JJ/IST440W/commits/master)

## Table of Contents

* [What is it?](#what-is-it)
* [Installation](#installation)
* [Issues?](#issues)
* [Documentation](#documentation)
* [Contributing](#contributing)
  * [Core Maintainers](#core-maintainers)
* [Licensing](#licensing)
  * [Third Party Licenses](#third-party-licenses)
* [Credits](#credits)

## What is it?
Team 1 is taking the initiative to analyze and decode handwritten notes and expand OCR capture capabilities solutions. The goal of the project is to decipher handwritten notes and incorporate automated decoding methods to facilitate computer forensic tools that would complete the process faster, accurately, and efficiently. 

## Issues?
Please see [Issues for IST440W-Decoder](https://gitlab.com/kevinW.JJ/IST440W/issues) and submit a new issue you are having.
	
## Installation
	1. git clone
	2. Run run_service.bat to start project

## Documentation
### Pre-requisites for Project

[MySQL Server](https://dev.mysql.com/downloads/mysql/)

[JDK 8+](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
	
### Hosting one locally?
	1. Import the schema into your own MySQL server.
	2. Update the application.properties with your own MySQL server.

## Licensing
[GNU GPL v3](https://gitlab.com/kevinW.JJ/IST440W/blob/master/LICENSE).

### Third Party Licenses
    None

### Core Maintainers
	- Kevin Jeong
	- Vita Stawski
	- Stephanie
	- Uri Martin
	- David Hyunh
	- Hector Garcia

## Credits
	- Spring Framework
	- MySQL
	- Apache Maven
	- Thymeleaf Template Engine