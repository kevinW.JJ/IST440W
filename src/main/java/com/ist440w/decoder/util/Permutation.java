/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ist440w.decoder.util;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 *
 * @author team1
 * 
 * This class will produce the possible combinations of column order and reorient each combination 
 * into a readable string if the result exists
 * 
 * @param <S> the elements of the list in string format
 */

public class Permutation<S> {
    private String strings = "";
    private ArrayList<String> totalSolutions = new ArrayList();
    private List<List<S>> columnPermutations = new ArrayList<>();
    /**
     * Initiates the permutation
     * @param possibleMatrix an input of an ArrayList of strings
     * @param key number of rows for each matrix input
     */
     public ArrayList initiatPermutation(List<S> possibleMatrix, int key){
        List<List<S>> output = new ArrayList();
        List<List<S>> possibleSolutions = new ArrayList<>();
        ArrayList<String> finalSolutions = new ArrayList();
        ArrayList possible = new ArrayList(possibleMatrix);

        for(int i = 0; i < possibleMatrix.size(); i++) {
            String str = possible.get(i).toString();
            output.add(buildRow(str, key));
        }
        for(int i = 0; i < output.size(); i ++) {
            possibleSolutions.addAll(permute(output.get(i)));
        }
        finalSolutions = permuteRows(possibleSolutions);

        output.clear();
        for(int i = 0; i < finalSolutions.size(); i++){
            getTotalSolutions().add(finalSolutions.get(i).toString());
        }
        finalSolutions.clear();
        return getTotalSolutions();
    }
    /**
     * the recursion process
     * @param permutation the temporary list input from permute
     * @param output the list containing the possible encrypted message
     * @return the embedded list
     */
     private List<List<S>> Permute(List<S> permutation, List<S> output) {

        if (output.size() <= 0) {
            getColumnPermutations().add(permutation);
            return getColumnPermutations();
        }
        for(S swap : output) {

            List<S> remnants = new ArrayList<S>(output);
            remnants.remove(swap);
            List<S> elements = new ArrayList<S>(permutation);
            elements.add(swap);
            Permute(elements, remnants);
        }
        return getColumnPermutations();
    }
    /**
     * The entry point into Permute and generates an embedded list
     * @param output is the list to be permuted
     * @return returns the embedded list finalSwap
     */
    public List<List<S>> permute(List<S> output) {
        List<S> permutation = new ArrayList<S>();
        List<List<S>> finalSwap = Permute(permutation, output);
        return finalSwap;
    }
    /**
     * Rearranges the matrix to produce an order of columns and rows that will be readable
     * if the solution is present
     * @param columnPermutations input of all of the permuted possible solutions
     * @return returns the list with all of the possible solutions
     */
    public ArrayList permuteRows(List<List<S>> columnPermutations){
        ArrayList<String> finalList = new ArrayList();
        for(int i = 0; i < columnPermutations.size(); i++){
           for(int j = 0;j < columnPermutations.get(i).get(0).toString().length(); j ++){
               for(int k = 0; k < columnPermutations.get(i).size(); k++){
                   char solutions =  columnPermutations.get(i).get(k).toString().charAt(j);
                   setStrings(getStrings() + solutions);
                }
           }
           StringBuilder delete = new StringBuilder(getStrings());
           for(int m = 0; m < getStrings().length(); m++){
               if(delete.charAt(m) == '_'){
                   delete.deleteCharAt(m);
                   m = 0;
                   setStrings(delete.toString());
               }
           }
           finalList.add(getStrings());
           setStrings(" ");
       }
       return finalList;
    }

    /**
     * Segregates the string into substrings for permutation
     * @param input the string to be broken into substrings
     * @param key the length of the substrings
     * @return a list of the substrings
     */
    public List<S> buildRow(String input, int key){
        String in = input;
        List<S> output = new ArrayList();
        int columnLength = (in.length()/key) ;
        int column = 0;
        int parseLength = columnLength ;
        int start = 0;
        String row = "";
        List<String> temp = new ArrayList();
        for(int i = 0; i < in.length(); i++){
            String temperary = "";
            if(i == parseLength && column < key) {
                parseLength = parseLength + columnLength;
                temperary = in.substring(start, i);
                temp.add(temperary);
                start = i;
                column++;
            } else if (column == key - 1){
                temperary = in.substring(start, in.length());
                temp.add(temperary);
                column++;
            }
        }
        for(int j = 0; j < temp.size(); j++){
            row = row + temp.get(j);
            output.add((S) row);
            row = "";
        }
        return output;
    }


    public String getStrings() {
        return strings;
    }

    public void setStrings(String strings) {
        this.strings = strings;
    }

    public ArrayList<String> getTotalSolutions() {
        return totalSolutions;
    }

    public void setTotalSolutions(ArrayList<String> totalSolutions) {
        this.totalSolutions = totalSolutions;
    }

    public List<List<S>> getColumnPermutations() {
        return columnPermutations;
    }

    public void setColumnPermutations(List<List<S>> columnPermutations) {
        this.columnPermutations = columnPermutations;
    }
}

