/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ist440w.decoder.util;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 *
 * @author team1
 * 
 * Underscore will be used to mimic an empty space when the matrix is uneven
 * 
 */
public class ColumnarTransposition <S>{

    int column;
    int row;
    private int initial;
    private String st = "";
    String input;
    LinkedHashSet<String> possibleMatrix = new LinkedHashSet<>();

    /**
     * Checks key versus string length for even substrings or odd substrings
     * 
     * @param input original string input
     */
     public List<S> initiateShifting(String input, int key){
        this.input = input;
        int checkForOddColumns = input.length()%key;
        if (checkForOddColumns != 0) {
            MatrixResolution(input, key);
        } else {
            possibleMatrix.add(input);
        }
        List<S> possible = new ArrayList(possibleMatrix);
        possibleMatrix.clear();
        return possible;
    }
     
    /**
     * Resolves uneven substrings 
     * 
     * @param input original string input to be deciphered
     * @param key will break the string into substrings
     */
    public void MatrixResolution(String input, int key){
        this.column = key;
        int rows = (input.length()%key);
        if(rows != 0){
            row = (input.length()/key) + 1;
        }else{
            row = (input.length()/key);
        }
        int matrix = row * column;
        int keyed = row;
        int emptyCell = matrix - input.length();
        int fullColumns = column - emptyCell;
        int done = 0;
        int i = 0;
        setInitial(row - 1);
        String finalString = "";
        while(i < input.length()){
            if(done != fullColumns){
                done++;
                finalString = finalString + input.substring(i, keyed);
            }else{
                keyed = keyed - 1;
                finalString = finalString + input.substring(i, keyed) + "_";
            }
            i = keyed;
            keyed = keyed + row;
        }

        possibleCombinationsColumns(finalString, input, key);
    }
    
    /**
     * n!/(r!(n-r))
     * 
     * @param length 
     * @return total number of possible combinations of ordered short substrings
     */
    public int calculateCombination(int length){
        int permuteCol = 1;
        int permuteEmpty = 1;
        int matrix = row * column;
        int empty = matrix - length;
        //permute calc for column
        for(int i = 0; i < column; i++){
            int temp = column;
            temp = temp - i;
            permuteCol = permuteCol * temp;
        }
        //permute calc for empty
        for(int i = 0; i < empty; i++){
            int temp = empty;
            temp = temp - i;
            permuteEmpty = permuteEmpty * temp;
        }
        //possible combination of short columns
        int combination = permuteCol / (permuteEmpty * (column - empty));
        return combination;
    }
    
    /**
     * creates a list of the possible combinations of substrings
     * 
     * @param possibleColumns the string modified with underscores (one possibility)
     * @param original is the original string
     * @param key is the number of rows to be checked
     */
     public void possibleCombinationsColumns(String possibleColumns, String original, int key){
        setSt(possibleColumns);
        int combination = calculateCombination(original.length());
        for(int i = 0; i < column - 1; i++){
            int mod = i%2;
            if(mod != 0){
                shiftLeft(possibleMatrix);
            }
            else{
                shiftRight(possibleMatrix);
            }
        }
        int matrixSize = possibleMatrix.size();
        int keyed = row;

        if(combination != matrixSize){
            addExtraUnderScore(possibleMatrix, matrixSize, keyed, key);
        }

    }
    /**
     * Adds extra underscore based on a comparison to the results of possible combinations and what was actually produced
     * @param possibleMatrix results from possibleCombinationsColumns
     * @param matrixSize size of the ArrayList possibleMatrix
     * @param keyed is a revised version of key to check the strings from the right side
     * @param key is the number of rows
     */
    public void addExtraUnderScore(LinkedHashSet<String> possibleMatrix, int matrixSize, int keyed, int key){
        ArrayList<String> possible = new ArrayList(possibleMatrix);
        for(int j = 0; j < possible.get(matrixSize - 1).length(); j++){
            if(possible.get(matrixSize-1).charAt(j) == '_' && j != key && j > keyed){
                StringBuilder delete = new StringBuilder(possible.get(matrixSize-1));
                delete.deleteCharAt(j);
                delete.insert(j - key, '_');
                possibleMatrix.add(delete.toString());
                j = possible.get(matrixSize - 1).length();
            }else{
                for(int k = possible.get(matrixSize-1).length() - 1; k >= 0; k--) {
                    if (possible.get(matrixSize - 1).charAt(k) == '_' && k > keyed) {
                        StringBuilder delete = new StringBuilder(possible.get(matrixSize - 1));
                        delete.deleteCharAt(k);
                        delete.insert(k - keyed, '_');
                        possibleMatrix.add(delete.toString());
                        j = possible.get(matrixSize - 1).length();
                        k = 0;
                    }
                }
            }

        }
    }
    /**
     * Method to set the underscores for the matrices 
     */
    public void handleUnderscore() {
        int placeHolder = getInitial();
        if (placeHolder > row) {
            int displace = placeHolder - row;
            int delete = 0;
            int temp = 0;
            int i = placeHolder;
            while (i < getSt().length()){
                if(getSt().charAt(i) == '_'){
                    temp = delete;
                    delete = i;
                    i = getSt().length();
                }
                i++;
            }
            if(temp != delete) {
                StringBuilder shiftTo = new StringBuilder(getSt());
                shiftTo.deleteCharAt(delete);
                shiftTo.insert(displace, '_');
                setSt(shiftTo.toString());
            }
        }
    }
    /**
     * Method to produce all possible combinations of the shortened columns from right to left
     * @param right the ArrayList that needs to be adjusted 
     */
    public void shiftRight(LinkedHashSet right) {
        handleUnderscore();
        right.add(getSt());
        for (int j = getInitial(); j < getSt().length(); j++) {
            if (getSt().charAt(j) == '_') {
                int i = j;
                while(i - row > getInitial()) {
                    if(getSt().charAt(i - row) != '_') {
                        StringBuilder remove = new StringBuilder(getSt());
                        remove.deleteCharAt(i);
                        String shift = remove.toString();
                        StringBuilder permute = new StringBuilder(shift);
                        permute.insert(i - row, '_');
                        setSt(permute.toString());
                        right.add(getSt());
                    }
                    i = i - row;
                }
            }
        }
        setInitial(getInitial() + row);
    }
    /**
     * Method to produce all possible combinations of the shortened columns from left to right
     * @param left the ArrayList that needs to be adjusted 
     */
    public void shiftLeft(LinkedHashSet left){
        handleUnderscore();
        left.add(getSt());
        for (int j = getSt().length() - 1; j > getInitial(); j--) {
            if(st.charAt(j) == '_') {
                int i = j;
                while(i + row < getSt().length()) {
                    if(getSt().charAt(i + row) != '_') {
                        StringBuilder remove = new StringBuilder(getSt());
                        remove.deleteCharAt(i);
                        String shift = remove.toString();
                        StringBuilder permute = new StringBuilder(shift);
                        permute.insert(i + row, '_');
                        setSt(permute.toString());
                        left.add(getSt());

                    }
                    i = i + row;
                }
            }
        }
        setInitial(getInitial() + row);
    }
    /*
    *the getters and setters
    */
    public int getInitial() {
        return initial;
    }
    public void setInitial(int initial) {
        this.initial = initial;
    }
    public String getSt() {
        return st;
    }
    public void setSt(String st) {
        this.st = st;
    }
}

