/*
 This class contains code utilized from online sources. These pieces of code will be utilized for the decryption portion of 
 Decoder, which includes the code for At Bash cipher, Caesar cipher, and Rail Fence Cipher. Full credit for the code is due 
 to the original authors.

 At bash Link:
 https://github.com/tmck-code/Ciphers/blob/master/Atbash.java

 Caesar Link:
 https://stackoverflow.com/questions/35240303/breaking-the-caesar-cipher

 Rail Fence Link:
 https://programmingcode4life.blogspot.com/2015/the-rail-cipher-and-how-to-decrypt.html

*/
package com.ist440w.decoder.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Team 1
 *
 */
public class BruteForce {
	
	/**
	 * Initialize and Set variables
	 */
	private static String LETTERS = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    @SuppressWarnings("serial")
	final private static ArrayList<String> LANGUAGES = new ArrayList<String>() {{
		add("src/main/resources/dictionary/NAMELIST.txt");
        add("src/main/resources/dictionary/ENGLISH.txt");
        add("src/main/resources/dictionary/DANISH.txt");
        add("src/main/resources/dictionary/DUTCH.txt");
        add("src/main/resources/dictionary/GERMAN.txt");
        add("src/main/resources/dictionary/ITALIAN.txt");
        add("src/main/resources/dictionary/NORWEGIAN.txt");
        add("src/main/resources/dictionary/SPANISH.txt");
        add("src/main/resources/dictionary/SWISS.txt");        
    }};

    /**
     * @param msg the encrypted message passes through here to try and get decrypted
     * @return best matching element from the results list of strings which contains all the cipher results
     * @throws IOException throws an error if dictionary files are not present
     */
    public String decipher(String msg) throws IOException {
    	//Check if already found
        HashSet<String> alreadyFound = new HashSet<String>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("src/main/resources/FOUND.txt"), "UTF-8"))) {
        	String line;
            // For each word in the input
            while((line = br.readLine())!= null){
                // Convert the word to lower case, trim it and insert into the set
            	alreadyFound.add(line.trim().toLowerCase());
            }
            // Close after done reading txt file.
            br.close();
        } catch (FileNotFoundException e) {
            System.out.println("Cannot find solution file.");
        } catch (IOException e) {
        	System.out.println("IOException error.");
        }
        if (!alreadyFound.isEmpty()) {
	        for(String ifFound : alreadyFound) {
	        	String[] split = ifFound.split(":");
	        	if (msg.equalsIgnoreCase(split[0])) {
	        		return split[1].toUpperCase();
	        	}
	        }
        }
        
    	AtbashCipher atbashBrute = new AtbashCipher();
    	CeasarCipher ceasarBrute = new CeasarCipher(msg);
    	RailFenceCipher railBrute = new RailFenceCipher(msg);
    	ColumnarTranspositionCipher columnTransposition = new ColumnarTranspositionCipher(msg);
        
    	String atbash = atbashBrute.decrypt(msg);
    	
    	//Start Threads
    	railBrute.start();
    	ceasarBrute.start();
    	columnTransposition.start();
    	
    	//Join
    	try {
    		railBrute.join();
    		ceasarBrute.join();
    		columnTransposition.join();
    	} catch (InterruptedException e) {
    		e.printStackTrace();
    	}
    	
    	
    	//Add Results to an ArrayList
    	ArrayList<String> results = new ArrayList<String>();
    	results.add(atbash);
    	results.addAll((ArrayList<String>)railBrute.result);
    	results.addAll((ArrayList<String>)ceasarBrute.result);
        results.addAll((ArrayList<String>)columnTransposition.result);
        
        //Remove Duplicates
        Set<String> hs = new HashSet<>();
        hs.addAll(results);
        results.clear();
        results.addAll(hs);
        
        findElementMatch findMatch = new findElementMatch(results);
        //Start Thread
        findMatch.start();
        
        try {
        	findMatch.join();
        } catch (InterruptedException e) {
        	e.printStackTrace();
        }
        try {
            Files.write(Paths.get("src/main/resources/FOUND.txt"), (msg+":"+findMatch.result+"\n").getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
        
        int[] clonedMatches = findMatch.dictMatches.clone();
        
        for(int x=0; x < findMatch.dictMatches.length; x++) {
        	System.out.println("Element " + x + ": " + clonedMatches[x] + " - " + findMatch.m.get(x));
        }
        System.out.println("HIGHEST: " + findMatch.largestIndex);
        
        System.out.println("DICTIONARY SIZE: " + findMatch.dictResults.size());

    	return findMatch.result;
    }
    

	/**
	 * @author Kevin
	 *
	 */
	public class findElementMatch extends Thread {
		ArrayList<String> m = new ArrayList<String>();
		ArrayList<String> dictResults = new ArrayList<String>();
		ArrayList<String> englishDictionary = new ArrayList<String>();
		int[] dictMatches = null;
		int largestIndex = 0;
		String result = "";
		
		/**
		 * @param m sets the Array
		 */
		public findElementMatch(ArrayList<String> m) {
			setDaemon(true);
			this.m = m;
		}
		
		public void run() {
	        	Dictionary EnglishDictionary = new Dictionary(LANGUAGES.get(1));
	        	EnglishDictionary.start();
	        	try {
	        		EnglishDictionary.join();
	        	} catch (InterruptedException e) {
	        		e.printStackTrace();
	        	}
	        	englishDictionary.addAll(EnglishDictionary.dictionary);
		    	// Grab all dictionary words into one dictionary set
		        for(int lang = 0; lang < LANGUAGES.size(); lang++) {
		        	Dictionary getDictionary = new Dictionary(LANGUAGES.get(lang));
		        	getDictionary.start();
		        	try {
		        		getDictionary.join();
		        		EnglishDictionary.join();
		        	} catch (InterruptedException e) {
		        		e.printStackTrace();
		        	}
		        	dictResults.addAll(getDictionary.dictionary);
		        }
	            Set<String> hs = new HashSet<>();
	            hs.addAll(dictResults);
	            dictResults.clear();
	            dictResults.addAll(hs);
	            
				dictMatches = new int[m.size()];
				if (m.get(0).trim().contains("\\s")) {
					PatternRunner patternRunner = new PatternRunner(m, dictResults);
					patternRunner.start();
					try {
						patternRunner.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			        getIndexOfLargest findLargest = new getIndexOfLargest(patternRunner.matches);
			        findLargest.start();
			        
			        try {
			        	findLargest.join();
			        } catch (Exception e) {
			        	e.printStackTrace();
			        }
			        this.largestIndex = findLargest.largest;
				} else {
					PatternRunner patternRunner = new PatternRunner(m, englishDictionary);
					patternRunner.start();
					try {
						patternRunner.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			        getIndexOfLargest findLargest = new getIndexOfLargest(patternRunner.matches);
			        findLargest.start();
			        
			        try {
			        	findLargest.join();
			        } catch (Exception e) {
			        	e.printStackTrace();
			        }
			        this.largestIndex = findLargest.largest;
				}
	        
	        result = m.get(this.largestIndex);
		}
	}
	
	/**
	 * Start of PatternRunner for matching patterns between dictionary and possible results
	 *
	 */
	public class PatternRunner extends Thread {
		ArrayList<String> m = null;
		ArrayList<String> dictResults = null;
		int[] matches = null;
		
		/**
		 * @param m sets the list of possible decrypted results
		 * @param dictResults sets the dictionary list
		 */
		public PatternRunner(ArrayList<String> m, ArrayList<String> dictResults) {
			setDaemon(true);
			this.m = m;
			this.dictResults = dictResults;
			this.matches = new int[m.size()];
		}
		
		public void run() {
			for (int key = 0; key < m.size(); key++) {
				for (String dictWord : dictResults) {
					String regex = "";
	        		if(m.get(key).trim().contains("\\s")) {
	        			regex = "\\W*(?i)\\b"+dictWord+"\\b(?-i)\\W*";
	        		} else {
	        			regex = "\\W*(?i)"+dictWord+"(?-i)\\W*";
	        		}
	        		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
	        		Matcher matcher = pattern.matcher(m.get(key).trim());
	        		if (matcher.find()) {
	        			matches[key] = matches[key] + 1;
	        		}
				}
			}
		}
	}
    

    /**
     * Dictionary grabber that will grab all words available and put them into a HashSet to remove any duplicates.
     *
     */
    public class Dictionary extends Thread {
    	String langPath = "";
    	HashSet<String> dictionary = new HashSet<String>();
    	
    	/**
    	 * @param langPath sets the language absolute path
    	 */
    	public Dictionary(String langPath) {
    		setDaemon(true); 
    		this.langPath = langPath;
    	}
    	public void run() {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(langPath), "UTF-8"))) {
            	String line;
                // For each word in the input
                while((line = br.readLine())!= null){
                    // Convert the word to lower case, trim it and insert into the set
                    dictionary.add(line.trim().toLowerCase());
                }
                // Close after done reading txt file.
                br.close();
            } catch (FileNotFoundException e) {
                System.out.println("Cannot find dictionary file.");
            } catch (IOException e) {
            	System.out.println("IOException error.");
            }
    	}
    }
    
    /**
     * Grabs the largest index of an array
     */
    public class getIndexOfLargest extends Thread {
    	int[] array = null;
    	int largest = 0;
    	
    	/**
    	 * @param array sets the array to look for the largest index
    	 */
    	public getIndexOfLargest(int[] array) {
    		setDaemon(true); 
    		this.array = array;
    	}
    	
    	public void run() {
    	      if ( array == null || array.length == 0 ) largest = -1; // null or empty
    	      
    	      for ( int i = 1; i < array.length; i++ )
    	      {
    	          if ( array[i] > array[largest] ) largest = i;
    	      }
    	}
    }
    
    /**
     * Start of Rail Cipher
     */
    public class RailFenceCipher extends Thread {
    	ArrayList<String> result = new ArrayList<String>();  //result is the returned value
    	String msg = "";
    	
    	/**
    	 * @param msg sets the message to decrypt
    	 */
    	public RailFenceCipher(String msg) {
    		setDaemon(true); 
    		this.msg = msg;
    	}
    	
    	public void run() {
	        for (int rails = 2; rails < msg.length(); rails++){  //loop for # of rails
	           
	            int key = rails;
	            String [][] parse = new String[key][msg.length()];
	           
	            for (int x = 0; x < parse.length; x++) {  //horizontal loop
	               
	                for (int y = 0; y < parse[x].length; y++) {  //vertical loop
	                   
	                    parse[x][y] = ".";  //populates parse with empty characters
	               
	                }
	            }
	           
	            //populate the fence
	            int count = 0;
	            int charMod = 1;
	            int even = 0;
	            int odd = 0;
	            int start = (key * 2) - 2;
	            even = start - 2;
	            odd = 2;
	           
	            for (int p = 0; p < parse.length; p++) {
	                charMod = 0;
	               
	                for (int p2 = p; p2 < parse[p].length;) {
	                    if (count != msg.length()) {
	                        if (p == 0 || p == key - 1) {
	                            parse[p][p2] = "" + msg.charAt(count);
	                            p2 = p2 + start;
	                        }
	                        else {
	                            parse[p][p2] = "" + msg.charAt(count);
	                            if (charMod % 2 == 0)
	                                p2 = p2 + even;
	                            else if (charMod % 2 == 1)
	                                p2 = p2 + odd;
	                            charMod++;
	                        }
	                        count++;
	                    }
	                    else break;
	                }
	               
	                if ((p != 0) && (p != (key - 1))) {
	                    even = even - 2;
	                    odd = odd + 2;
	                        }
	                       
	            }
	                   
	            int move = 1;
	            count = 0;
	            String z = "";
	           
	            for (int p = 0; p < msg.length(); p++) {
	                if ((move % 2) != 0) {
	                    z = z + parse[count][p];
	                    if (count == (key - 1)) {
	                        move = 2;
	                        count = key - 2;
	                    }
	                    else count++;
	                }
	                else if ((move % 2 ) == 0) {
	                    z = z + parse[count][p];
	                    if (count == 0) {
	                        move = 1;
	                        count = 1;
	                    }
	                    else count--;
	                }
	            }
	            
	            result.add(z);
	        }
            Set<String> hs = new HashSet<>();
            hs.addAll(result);
            result.clear();
            result.addAll(hs);
    	}
    }
	/**
	 * End of Rail Cipher
	 */
	/**
	 * Start of Ceasar Cipher
	 */
    public class CeasarCipher extends Thread {
    	ArrayList<String> result = new ArrayList<String>();
    	String msg = "";
    	
    	/**
    	 * @param msg sets the message to decrypt
    	 */
    	public CeasarCipher(String msg) {
    		setDaemon(true); 
    		this.msg = msg;
    	}
    	
    	public void run() {
			msg = msg.toUpperCase();
			char msg_array[] = msg.toCharArray();
			for (int key = 0; key < LETTERS.length(); key++) {
				String trans = "";
				for (char a : msg_array) {
					if(LETTERS.indexOf(a) != -1) {
						int num = LETTERS.indexOf(a);
						num -= key; 
						if (num < 0) num += LETTERS.length();
						trans += LETTERS.charAt(num);
					} else trans += a;
				}
				result.add(trans);
			}
            Set<String> hs = new HashSet<>();
            hs.addAll(result);
            result.clear();
            result.addAll(hs);
    	}
    }
	/**
	 * End of Ceasar Cipher
	 */
	/**
	 * Start of Atbash Cipher
	 */
	public class AtbashCipher {
        private String decrypt(String msg) {
            String clearText = "";
            for(Character c: msg.toLowerCase().toCharArray())
                clearText += cipher(c);
            return clearText;
        }
        private String cipher(Character c) {
            if('0' <= c && c <= '9')
                return String.valueOf(c);
            else if ('a' <= c && c <= 'z')
                return String.valueOf((char) (219 - (int) c));
            else
                return " ";
        }
	}
	/**
	 * End of Atbash Cipher
	 */
    /**
     * Start Columnar Transposition
 	 *
     */
    public class ColumnarTranspositionCipher extends Thread {
    	ArrayList<String> result = new ArrayList<String>();
    	String msg = "";
    	
    	/**
    	 * @param msg sets the message to decrypt
    	 */
    	public ColumnarTranspositionCipher(String msg) {
    		setDaemon(true); 
    		this.msg = msg;
    	}
    	
    	public void run() {
            Permutation<String> per = new Permutation();
            ColumnarTransposition columnTrans = new ColumnarTransposition();
            List<List> columns = new ArrayList();
            int maxKey = 6;
            for (int i = 2; i < maxKey; i++) {
                List keyed = new ArrayList();
                keyed.addAll(columnTrans.initiateShifting(msg, i));
                columns.add(keyed);
                ArrayList<String> permuted = per.initiatPermutation((List<String>) columns.get(i - 2), i);
                permuted.replaceAll(String::trim);
                result.addAll(permuted);
                Set<String> hs = new HashSet<>();
            }
            Set<String> hs = new HashSet<>();
            hs.addAll(result);
            result.clear();
            result.addAll(hs);
    	}
    }
    
    /**
     * End Columnar Transposition
     */
}
