package com.ist440w.decoder.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ist440w.decoder.model.UserDocument;
import com.ist440w.decoder.repository.UserDocumentRepository;
import com.ist440w.decoder.service.Interface.UserDocumentService;

/**
 * @author Team 1
 *
 */
@Service("userDocumentService")
public class UserDocumentServiceImpl implements UserDocumentService {
	@Autowired
	private UserDocumentRepository userDocumentRepository;

	@Override
	public List<UserDocument> findAll() {
		return userDocumentRepository.findAll();
	}

	@Override
	public UserDocument findById(int id) {
		return userDocumentRepository.findOne(id);
	}

	@Override
	public void save(UserDocument document) {
		userDocumentRepository.save(document);
	}

	@Override
	public List<UserDocument> findAllByUserId(int userId) {
		return userDocumentRepository.findAllByUserId(userId);
	}

	@Override
	public void deleteById(int id) {
		userDocumentRepository.delete(id);
	}
	


    
}
