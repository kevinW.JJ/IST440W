package com.ist440w.decoder.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ist440w.decoder.model.Log;
import com.ist440w.decoder.model.User;
import com.ist440w.decoder.model.UserDocument;
import com.ist440w.decoder.repository.LoggingRepository;
import com.ist440w.decoder.service.Interface.LoggingService;

/**
 * @author Team 1
 *
 */
@Service("loggingService")
public class LoggingServiceImpl implements LoggingService {
	
	@Autowired
	private LoggingRepository loggingRepository;

	@Override
	public void save(Log log) {
		loggingRepository.save(log);
	}
	
	public void save(Log log, User user) {
		user.getName();
		Log newLog = new Log();
		newLog.setLogMessage("[USER: " + user.getEmail() + "] " + log.getLogMessage());
		newLog.setType(log.getType());
		newLog.setTimeStamp(new Timestamp(System.currentTimeMillis()));
		loggingRepository.save(newLog);
	}
	
	public void save(Log log, User user, UserDocument document) {
		user.getName();
		Log newLog = new Log();
		newLog.setLogMessage("[USER: " + user.getEmail() + "] " + log.getLogMessage() + "[File: " + document.getName() + "]");
		newLog.setType(log.getType());
		newLog.setTimeStamp(new Timestamp(System.currentTimeMillis()));
		loggingRepository.save(newLog);
	}

	@Override
	public Log findById(int id) {
		return loggingRepository.findById(id);
	}

	@Override
	public List<Log> findAll() {
		return loggingRepository.findAll();
	}

	@Override
	public void deleteById(int id) {
		loggingRepository.delete(id);
	}
}
