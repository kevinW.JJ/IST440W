package com.ist440w.decoder.service;

import java.util.List;

import com.ist440w.decoder.model.Log;
import com.ist440w.decoder.model.User;
import com.ist440w.decoder.model.UserDocument;

/**
 * @author Team 1
 *
 */
public class Interface {
	
	/**
	 * UserService Interface
	 */
	public interface UserService {
		/**
		 * @param email is the email address of the user
		 * @return the User by their email
		 */
		public User findUserByEmail(String email);
		/**
		 * @param userid is the id of the user
		 * @return the User by their id
		 */
		public User findUserById(int userid);
		/**
		 * @return all the list of users
		 */
		public List<User> findAllUsers();
		/**
		 * @param user saves the user into the database
		 */
		public void saveUser(User user);
	}
	
	/**
	 * UserDocumentService Interface
	 */
	public interface UserDocumentService {
		/**
		 * @return all the uploaded documents
		 */
		List<UserDocument> findAll();
		/**
		 * @param id finds file by it's identifier
		 * @return the file by it's id
		 */
		UserDocument findById(int id);
		/**
		 * @param document saves file into the database
		 */
		void save(UserDocument document);
		/**
		 * @param userId finds all documents owned by user
		 * @return all the files uploaded by userId
		 */
		List<UserDocument> findAllByUserId(int userId);
		/**
		 * @param id deletes the file by it's id
		 */
		void deleteById(int id);
	}
	
	/**
	 * LoggingService Interface
	 */
	public interface LoggingService {
		/**
		 * @param log saves the Log into the database
		 */
		void save(Log log);
		/**
		 * @param log saves the Log into the database
		 * @param user saves the User into the Log with the database
		 */
		void save(Log log, User user);
		/**
		 * @param log saves the Log into the database
		 * @param user saves the User into the Log with the database
		 * @param document saves the File into the User with the database
		 */
		void save(Log log, User user, UserDocument document);
		/**
		 * @param id searches Log by id
		 * @return the log by id
		 */
		Log findById(int id);
		/**
		 * @return the list of all logs
		 */
		List<Log> findAll();
		/**
		 * @param id delete the Log by it's id
		 */
		void deleteById(int id);
	}
}

