package com.ist440w.decoder.controller;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.translate.Translate;
import com.google.api.services.translate.Translate.Translations;
import com.google.api.services.translate.TranslateScopes;
import com.google.api.services.translate.model.TranslationsListResponse;
import com.google.api.services.translate.model.TranslationsResource;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.Vision.Builder;
import com.google.api.services.vision.v1.VisionScopes;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.Image;
import com.google.common.collect.ImmutableList;
import com.ist440w.decoder.model.FileBucket;
import com.ist440w.decoder.model.Log;
import com.ist440w.decoder.model.User;
import com.ist440w.decoder.model.UserDocument;
import com.ist440w.decoder.service.Interface.LoggingService;
import com.ist440w.decoder.service.Interface.UserDocumentService;
import com.ist440w.decoder.service.Interface.UserService;
import com.ist440w.decoder.util.BruteForce;
import com.ist440w.decoder.util.FileValidator;




/**
 * @author Team 1
 *
 */
@Controller
public class AppController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserDocumentService userDocumentService;
	
	@Autowired
	private LoggingService loggingService;
	
	@Autowired
	private FileValidator fileValidator;
	
	@InitBinder("fileBucket")
	protected void initBinder(WebDataBinder binder) {
	   binder.setValidator(fileValidator);
	}
	
	private Log OCR_ERROR = createLog("ERROR", "Could not make out the words in the image or file not found. Try a different one.");
	private Log NP_ERROR = createLog("ERROR", "NullPointerException Image might not be a good one. Try a different one.");
	private Log DOCUMENT_ERROR = createLog("ERROR", "That document doesn't exist or using wrong identifiers!");
	private Log DOCUMENT_TYPE_ERROR = createLog("ERROR", "Tried to upload the wrong content type!");
	private Log DOCUMENT_INFO = createLog("INFO", "Uploaded a file.");
	private Log DATA_INFO = createLog("INFO", "There's already a record that exists.");
	private Log REGISTRATION_ERROR = createLog("ERROR", "Tried to register an account that is already on the system.");
	private Log GENERAL_ERROR = createLog("ERROR", "Oh no! Something went wrong..");
	private Log LOGIN_INFO = createLog("INFO", "Successfully logged in.");
	private Log LOGOUT_INFO = createLog("INFO", "Successfully logged out.");
	private Log VALIDATION_ERROR = createLog("ERROR", "File failed to validate.");
	private Log DELETE_INFO = createLog("INFO", "Deleted a file.");
	
	private final String oneLiner = "^[A-Za-z]*$";

	/**
	 * @return list all existing users.
	 */
	@RequestMapping(value = { "/list" }, method = RequestMethod.GET)
	public ModelAndView listUsers() {
		ModelAndView modelAndView = new ModelAndView();
		List<User> users = userService.findAllUsers();
		modelAndView.addObject("users", users);
		modelAndView.setViewName("list");
		return modelAndView;
	}
	
	
	/**
	 * @param userId is the currently logged in user at /user/home
	 * @param docId is the document id that the user is targeting for
	 * @param targetLang is the target language that the user is targeting for
	 * @return the possible decrypted message with translation
	 */
	@RequestMapping(value = { "/usedoc-{userId}-{docId}-{targetLang}" }, method = RequestMethod.GET)
	public ModelAndView useDocument(@PathVariable int userId, @PathVariable int docId, @PathVariable String targetLang) {
		ModelAndView modelAndView = new ModelAndView(new RedirectView("user/home"));
		try {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth.isAuthenticated() && auth != null) {
				User user = userService.findUserById(userId);
				User userCalled = userService.findUserByEmail(auth.getName());
				UserDocument document = userDocumentService.findById(docId);
				List<UserDocument> userDocuments = userDocumentService.findAll();
				modelAndView.setViewName("user/home");
				List<UserDocument> documents = userDocumentService.findAll();
				

				userCalled.setTargetLanguage(targetLang);

				modelAndView.addObject("documents", documents);
				modelAndView.addObject("userId",user.getId());
				modelAndView.addObject("users", userService);
				modelAndView.addObject("documentsList",userDocuments);
				
				//Set Encrypted Message and Decrypted Message and Translated Message
				if (targetLang.equalsIgnoreCase("undefined") || targetLang.isEmpty()) {
					modelAndView.addObject("encryptedMessage","Please select a target language before using the specific file.");
					modelAndView.setViewName("user/home"); 
					return modelAndView;
				} else {
					OCRFileAndDecryptAndTranslate(document, userCalled);
				}
				
		    	Locale target = new Locale(userCalled.getTargetLanguage());
		    	Pattern pattern = Pattern.compile(oneLiner, Pattern.CASE_INSENSITIVE);
		    	Matcher matcher = pattern.matcher(userCalled.getDecryptedString());
		    	if (matcher.find()) {
		    		modelAndView.addObject("encryptedMessage", userCalled.getEncryptedString());
		            modelAndView.addObject("resultMessage", userCalled.getDecryptedString());
		            modelAndView.setViewName("user/home"); 
		            return modelAndView;
		    	}
				
		    	//userService.saveUser(userCalled);
				modelAndView.addObject("encryptedMessage", userCalled.getEncryptedString());
		        modelAndView.addObject("resultMessage", userCalled.getDecryptedString());
		        modelAndView.addObject("translatedMessage", userCalled.getTranslatedString());
		        modelAndView.addObject("sourceLanguage", userCalled.getSourceLanguage());
		        modelAndView.addObject("targetLanguage", target.getDisplayLanguage(Locale.ENGLISH));
		        modelAndView.setViewName("user/home"); 
			} else {
				modelAndView.setViewName("login");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
 
 		return modelAndView;
	}
    
    /**
     * @param document is the file to OCR
     * @param user is the user that called the OCR
     * Method OCR's image, decrypts, and translates
     */
	private void OCRFileAndDecryptAndTranslate(UserDocument document, User user) {
		//Google Credentials
		InputStream credentials = getClass().getClassLoader().getResourceAsStream("auth.json");
		JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
		HttpTransport httpTransport;
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			GoogleCredential credential = GoogleCredential.fromStream(credentials).createScoped(VisionScopes.all()).createScoped(TranslateScopes.all());
			credential.refreshToken();
			Builder vision = new Vision.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName("decoder");
			Translate.Builder translator = new Translate.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName("decoder");
			ImmutableList.Builder<AnnotateImageRequest> requests = ImmutableList.builder();
	        requests.add(
	                new AnnotateImageRequest()
	                        .setImage(new Image().encodeContent(document.getContent()))
	                        .setFeatures(ImmutableList.of(
	                                new Feature()
	                                        .setType("DOCUMENT_TEXT_DETECTION")
	                                        .setMaxResults(1)
	                        ))
	        );
								
	        Vision.Images.Annotate annotate =
	                vision.build().images().annotate(new BatchAnnotateImagesRequest().setRequests(requests.build()));
	        
	        
	        annotate.setDisableGZipContent(true);
	        BatchAnnotateImagesResponse batchResponse = annotate.execute();
	        batchResponse.getResponses().forEach(action -> {
	        	List<EntityAnnotation> entityAnnotations = action.getTextAnnotations();
	        	if(entityAnnotations.stream().findFirst().isPresent()) {
	        		try {
	        			if (entityAnnotations.stream().findFirst().get().getDescription() == null || entityAnnotations.stream().findFirst().get().getDescription().isEmpty()) {
	        				loggingService.save(OCR_ERROR, user);
	        			} else {
	        				user.setEncryptedString(entityAnnotations.stream().findFirst().get().getDescription());
	        						BruteForce bruteForce = new BruteForce();
	        				Pattern pattern = Pattern.compile("[^\\s]([ ]{1,})");
	        				Matcher matcher = pattern.matcher(user.getEncryptedString());
	        				if (matcher.find()) {
	        					user.setEncryptedString(entityAnnotations.stream().findFirst().get().getDescription().trim().replaceAll("\n", " "));
	        				} else {
	        					user.setEncryptedString(entityAnnotations.stream().findFirst().get().getDescription().trim().replaceAll("\\s",""));
	        				}
	        				String toDecrypt = bruteForce.decipher(user.getEncryptedString());
	        				user.setDecryptedString(toDecrypt);
	        				System.out.println("TO BE DECIPHERED: " + user.getDecryptedString());
	        				
	        				System.out.println("BOOLEAN REGEX: " + matcher.find());
	        			}
					} catch (DataIntegrityViolationException e) {
						e.printStackTrace();
						loggingService.save(DATA_INFO, user);
					} catch (ArrayIndexOutOfBoundsException e) {
						loggingService.save(GENERAL_ERROR, user);
					} catch (ConcurrentModificationException e) {
						e.printStackTrace();
						loggingService.save(DATA_INFO, user);
					} catch (NullPointerException e) {
						e.printStackTrace();
						loggingService.save(NP_ERROR ,user);
					} catch (IOException e) {
						e.printStackTrace();
						loggingService.save(DOCUMENT_ERROR, user);
					}
	        	}
	        });
	        

	        Translate t = translator.build();
	        Translations.List list = t.new Translations().list(
	        		Arrays.asList(user.getDecryptedString()),
	        		user.getTargetLanguage().toUpperCase());
	        TranslationsListResponse tlist = list.execute();
	        for (TranslationsResource tr : tlist.getTranslations()) {
	        	Locale loc = new Locale(tr.getDetectedSourceLanguage());
	        	user.setSourceLanguage(loc.getDisplayLanguage(new Locale("en", "EN")));
	        	user.setTranslatedString(tr.getTranslatedText());
	        	break;
	        }

		} catch (IOException e) {
			loggingService.save(GENERAL_ERROR, user);
		} catch (GeneralSecurityException e) {
			loggingService.save(GENERAL_ERROR, user);
		} catch (DataIntegrityViolationException e) {
			loggingService.save(DATA_INFO, user);
		} catch (ArrayIndexOutOfBoundsException e) {
			loggingService.save(GENERAL_ERROR, user);
		}
	}
	
    /**
     * @param userId is the id of the user who owns the document/file
     * @param docId is the id of the document/file to delete
     * @return deletes the document that is by userId and docId
     */
    @RequestMapping(value = { "/delete-document-{userId}-{docId}" }, method = RequestMethod.GET)
    public ModelAndView removeDocument(@PathVariable int userId, @PathVariable int docId) {
    	ModelAndView modelAndView = new ModelAndView(new RedirectView("user/home"));
		try {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth.isAuthenticated() && auth != null) {
				UserDocument document = userDocumentService.findById(docId);
				userDocumentService.deleteById(docId);
				List<UserDocument> userDocuments = userDocumentService.findAll();
				modelAndView.setViewName("user/home");;
				User user = userService.findUserById(userId);
				List<UserDocument> documents = userDocumentService.findAllByUserId(userId);
				modelAndView.addObject("documents", documents);
				modelAndView.addObject("userId",user.getId());
				modelAndView.addObject("users", userService);
				modelAndView.addObject("documentsList",userDocuments);
				user.setStatus("SUCCESSFULLY DELETED");
				modelAndView.addObject("errormessage", user.getStatus());
				loggingService.save(DELETE_INFO, user, document);
			} else {
				modelAndView = new ModelAndView(new RedirectView("login"));
				modelAndView.setViewName("login");
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return modelAndView;
    }
    
	/**
	 * @param fileBucket is the file that is being uploaded
	 * @param result is the form's binding result from submission
	 * @param userId is the currently logged in user uploading the file
	 * @return adds the document that is by userId and generates a docId
	 * @throws IOException if the file is invalid
	 */
	@RequestMapping(value = { "/add-document-{userId}" }, method = RequestMethod.POST)
	public ModelAndView uploadDocument(@Valid FileBucket fileBucket, BindingResult result, @PathVariable int userId) throws IOException{
		ModelAndView modelAndView = new ModelAndView(new RedirectView("user/home"));
		List<UserDocument> userDocuments = userDocumentService.findAll();
		if (result.hasErrors()) {
			User user = userService.findUserById(userId);
			modelAndView.addObject("user", user);
			List<UserDocument> documents = userDocumentService.findAllByUserId(user.getId());
			user.setStatus("No file selected!");
			modelAndView.addObject("errormessage", user.getStatus());
			modelAndView.addObject("documents", userDocuments);
			modelAndView.addObject("userId",user.getId());
			modelAndView.addObject("users", userService);
			modelAndView.addObject("documentsList",documents);
			
			loggingService.save(VALIDATION_ERROR, user);

			return modelAndView;
		} else {
			User user = userService.findUserById(userId);
			modelAndView.addObject("user", user);
			MultipartFile multipartFile = fileBucket.getFile();
			if (multipartFile.getContentType().equalsIgnoreCase("image/png") || multipartFile.getContentType().equalsIgnoreCase("image/jpeg") || multipartFile.getContentType().equalsIgnoreCase("image/bmp") || multipartFile.getContentType().equalsIgnoreCase("image/svg+xml")) {
			
				saveDocument(fileBucket, user);
				
				
				UserDocument document = new UserDocument();
				document.setName(multipartFile.getOriginalFilename());
				document.setDescription(fileBucket.getDescription());
				document.setType(multipartFile.getContentType());
				document.setContent(multipartFile.getBytes());
				document.setUser(user);
				
				user.setStatus("SUCCESS!");
				loggingService.save(DOCUMENT_INFO, user, document);
				modelAndView.addObject("errormessage", user.getStatus());
				modelAndView.addObject("userId",user.getId());
				modelAndView.addObject("users", userService);
				modelAndView.addObject("documentsList",userDocuments);
				
		
				return modelAndView;
			} else {
				UserDocument document = new UserDocument();
				document.setName(multipartFile.getOriginalFilename());
				document.setDescription(fileBucket.getDescription());
				document.setType(multipartFile.getContentType());
				document.setContent(multipartFile.getBytes());
				document.setUser(user);
				
				loggingService.save(DOCUMENT_TYPE_ERROR, user, document);
				
				user.setStatus("FileType Error - Please upload a PNG, JPEG, BMP, or SVG+XML");
				modelAndView.addObject("errormessage", user.getStatus());
				modelAndView.addObject("userId",user.getId());
				modelAndView.addObject("users", userService);
				modelAndView.addObject("documentsList",userDocuments);
				return modelAndView;
			}
		}
	}
	
	/**
	 * 
	 * @param type creates a Log with a specific type to declare severity (e.g. INFO, DEBUG, CRITICAL)
	 * @param logMessage creates a Log with a specific message
	 * @return creates a Log model
	 */
	private Log createLog(String type, String logMessage) {
		Log log = new Log();
		Timestamp time = new Timestamp(System.currentTimeMillis());
		log.setTimeStamp(time);
		log.setType(type);
		log.setLogMessage(logMessage);
		return log;
	}
	
	/**
	 * 
	 * @param fileBucket is the file to save
	 * @param user is the user who uploaded the file
	 * @throws IOException if the file is invalid
	 * This saves the document into the user_document which holds the files in a BLOB type format.
	 */
	private void saveDocument(FileBucket fileBucket, User user) throws IOException{
		
		UserDocument document = new UserDocument();
		
		MultipartFile multipartFile = fileBucket.getFile();
		
		document.setName(multipartFile.getOriginalFilename());
		document.setDescription(fileBucket.getDescription());
		document.setType(multipartFile.getContentType());
		document.setContent(multipartFile.getBytes());
		document.setUser(user);
		userDocumentService.save(document);
	}
	
	/**
	 * @return the splash page or landing page
	 */
	@RequestMapping(value="/", method = RequestMethod.GET)
	public ModelAndView splash() {
		ModelAndView modelAndView = new ModelAndView(new RedirectView("user/home"));
		try {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth.isAuthenticated() && auth != null) {
				User user = userService.findUserByEmail(auth.getName());
				user.setStatus("");
				loggingService.save(LOGIN_INFO, user);
				List<UserDocument> userDocuments = userDocumentService.findAllByUserId(user.getId());
				List<UserDocument> documents = userDocumentService.findAll();
				modelAndView.addObject("documents", userDocuments);
				modelAndView.addObject("userId",user.getId());
				modelAndView.addObject("users", userService);
				modelAndView.addObject("documentsList",documents);
				modelAndView.setViewName("user/home");
			} else {
				modelAndView.setViewName("index");
			}
		} catch (NullPointerException e) {
			modelAndView.setViewName("index");	
		}
		
		return modelAndView;
	}
	
	/**
	 * @return the about us page
	 */
	@RequestMapping(value="/about", method = RequestMethod.GET)
	public ModelAndView team() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("about");
		return modelAndView;
	}
	
	/**
	 * @return the login page
	 */
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public ModelAndView login(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	/**
	 * @return the logout page
	 */
	@RequestMapping(value="/signout", method = RequestMethod.GET)
	public ModelAndView logoutPage () {
		ModelAndView modelAndView = new ModelAndView();
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    User user = userService.findUserByEmail(auth.getName());
	    if (auth != null) {
	    	loggingService.save(LOGOUT_INFO, user);
	    	SecurityContextHolder.getContext().setAuthentication(null);
	    }
	    modelAndView.setViewName("login");
	    return modelAndView;
	}
	
	
	/**
	 * @return registration page
	 */
	@RequestMapping(value="/registration", method = RequestMethod.GET)
	public ModelAndView registration(){
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}
	
	/**
	 * @param user is the result from the form
	 * @param bindingResult will show the results if the form is valid or invalid
	 * @return registration submit(POST) which will result in an error or success
	 */
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult
					.rejectValue("email", "error.user",
							"There is already a user registered with the email provided");
			loggingService.save(REGISTRATION_ERROR, user);
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("registration");
			
		}
		return modelAndView;
	}
	
	/**
	 * @param imageId is the id of the document/file
	 * @return the image content
	 */
	@RequestMapping(value = "image/{imageId}")
	@ResponseBody
	public byte[] getImage(@PathVariable int imageId) {
		return userDocumentService.findById(imageId).getContent();
	}
	
	/**
	 * @return the user's home page after logging in
	 */
	@RequestMapping(value="/user/home", method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		if (auth.isAuthenticated()) {
			loggingService.save(LOGIN_INFO, user);
			user.setStatus("");
		}
		List<UserDocument> userDocuments = userDocumentService.findAll();

		modelAndView.addObject("errormessage", user.getStatus());
		modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
		modelAndView.addObject("userId",user.getId());
		modelAndView.addObject("users", userService);
		modelAndView.addObject("documentsList",userDocuments);
		modelAndView.setViewName("user/home");
		return modelAndView;
	}
	

}
