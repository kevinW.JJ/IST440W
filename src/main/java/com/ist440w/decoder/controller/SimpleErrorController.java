package com.ist440w.decoder.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author Team 1
 *
 */
@RestController
@RequestMapping("${server.error.path:${error.path:/error}}")
public class SimpleErrorController implements ErrorController {

	private final ErrorAttributes errorAttributes;

	private static final String ERROR_PATH = "/error";

	/**
	 * @param errorAttributes Provides access to error attributes which can be logged or presented to the user.
	 * sets the errorAttributes
	 */
	@Autowired
	public SimpleErrorController(ErrorAttributes errorAttributes) {
		Assert.notNull(errorAttributes, "ErrorAttributes must not be null");
		this.errorAttributes = errorAttributes;
	}

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}

	/**
	 * @param aRequest the source request attributes
	 * @return a map of error attributes
	 */
	@RequestMapping
	public Map<String, Object> error(HttpServletRequest aRequest) {
		return getErrorAttributes(aRequest, getTraceParameter(aRequest));
	}

	private boolean getTraceParameter(HttpServletRequest request) {
		String parameter = request.getParameter("trace");
		if (parameter == null) {
			return false;
		}
		return !"false".equals(parameter.toLowerCase());
	}

	private Map<String, Object> getErrorAttributes(HttpServletRequest aRequest, boolean includeStackTrace) {
		RequestAttributes requestAttributes = new ServletRequestAttributes(aRequest);
		return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
	}

}