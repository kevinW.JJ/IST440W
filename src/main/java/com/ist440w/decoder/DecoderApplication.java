package com.ist440w.decoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Team 1
 *
 */
@SpringBootApplication
public class DecoderApplication {

	/**
	 * @param args Runs the spring application
	 */
	public static void main(String[] args) {
		SpringApplication.run(DecoderApplication.class, args);
	}
}
