package com.ist440w.decoder.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ist440w.decoder.model.Role;

/**
 * @author Team 1
 * JPA Repository that handles persisting Java objects into relational databases
 */
@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role, Integer>{
	/**
	 * @param role is the string name of the role to search for the Role
	 * @return the Role by the role's name
	 */
	Role findByRole(String role);
}