package com.ist440w.decoder.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ist440w.decoder.model.Log;

/**
 * @author Team 1
 * JPA Repository that handles persisting Java objects into relational databases
 */
@Repository("loggingRepository")
public interface LoggingRepository extends JpaRepository<Log, Integer>{
	/**
	 * @param id is the id of the Log to search for the Log
	 * @return the Log by it's id
	 */
	Log findById(int id);
}
