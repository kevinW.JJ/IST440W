package com.ist440w.decoder.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ist440w.decoder.model.UserDocument;

/**
 * @author Team 1
 * JPA Repository that handles persisting Java objects into relational databases
 */
@Repository("userDocumentRepository")
public interface UserDocumentRepository extends JpaRepository<UserDocument, Integer> {
	 /**
	 * @param id is the file's id to search for the UserDocument
	 * @return the UserDocument by it's id
	 */
	UserDocument findById(int id);
	 /**
	 * @param id is the list of all files identified by a user's id
	 * @return the list of all documents uploaded by user's id
	 */
	List<UserDocument> findAllByUserId(int id);
}