package com.ist440w.decoder.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ist440w.decoder.model.User;

/**
 * @author Team 1
 * JPA Repository that handles persisting Java objects into relational databases
 */
@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {
	 /**
	 * @param email is the user's email to search for the User
	 * @return the User by their email address
	 */
	User findByEmail(String email);
	 /**
	 * @param id is the user id's to search for the User
	 * @return the User by their id
	 */
	User findById(int id);
}