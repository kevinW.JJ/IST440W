package com.ist440w.decoder.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;

/**
 * @author Team 1
 *
 */
@Entity
@Table(name = "user")
@DynamicUpdate
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private int id;
	@Column(name = "email")
	@Email(message = "*Please provide a valid Email")
	@NotEmpty(message = "*Please provide an email")
	private String email;
	@Column(name = "password")
	@Length(min = 5, message = "*Your password must have at least 5 characters")
	@NotEmpty(message = "*Please provide your password")
	@Transient
	private String password;
	@Column(name = "name")
	@NotEmpty(message = "*Please provide your name")
	private String name;
	@Column(name = "last_name")
	@NotEmpty(message = "*Please provide your last name")
	private String lastName;
	@Column(name = "active")
	private int active;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;
	//Results
	@Column(name="encrypted_string", length=255, nullable=true)
	private String encryptedString;
	@Column(name="decrypted_string", length=255, nullable=true)
	private String decryptedString;
	@Column(name="translated_string", length=255, nullable=true)
	private String translatedString;
	@Column(name="source_language", length=255, nullable=true)
	private String sourceLanguage;
	@Column(name="target_language", length=255, nullable=true)
	private String targetLanguage;
	@Column(name="status", length=255, nullable=true)
	private String status;

	/**
	 * @return id of the user
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id sets the id of the user
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return password of user that is encrypted
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password sets the password of the user
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the first name of the user
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name sets the first name of the user
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the last name of the user
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName sets the last name of the user
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email of the user
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email sets the email of the user
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return if user is active account
	 */
	public int getActive() {
		return active;
	}

	/**
	 * @param active (0|1)
	 * 0: Deactivate
	 * 1: Activate
	 */
	public void setActive(int active) {
		this.active = active;
	}

	/**
	 * @return the available roles
	 */
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * @param roles sets the roles of the user
	 */
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	/**
	 * @return the encrypted string result
	 */
	public String getEncryptedString() {
		return encryptedString;
	}

	/**
	 * @param encryptedString to set the encrypted string result
	 */
	public void setEncryptedString(String encryptedString) {
		this.encryptedString = encryptedString;
	}

	/**
	 * @return the decrypted string result
	 */
	public String getDecryptedString() {
		return decryptedString;
	}

	/**
	 * @param decryptedString to set the decrypted string result
	 */
	public void setDecryptedString(String decryptedString) {
		this.decryptedString = decryptedString;
	}

	/**
	 * @return the translated string result
	 */
	public String getTranslatedString() {
		return translatedString;
	}

	/**
	 * @param translatedString to set the translated string result
	 */
	public void setTranslatedString(String translatedString) {
		this.translatedString = translatedString;
	}

	/**
	 * @return the source language of decrypted string result
	 */
	public String getSourceLanguage() {
		return sourceLanguage;
	}

	/**
	 * @param sourceLanguage set the source language of decrypted result
	 */
	public void setSourceLanguage(String sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	/**
	 * @return the target language to translate to from decrypted string
	 */
	public String getTargetLanguage() {
		return targetLanguage;
	}

	/**
	 * @param targetLanguage sets the target language to translate to from decrypted string
	 */
	public void setTargetLanguage(String targetLanguage) {
		this.targetLanguage = targetLanguage;
	}

	/**
	 * @return the status of user on page
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status sets the status of user on page
	 * Can be informational messages for user to understand if something happened
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
