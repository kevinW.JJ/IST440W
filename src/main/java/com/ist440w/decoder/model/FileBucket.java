package com.ist440w.decoder.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Team 1
 *
 */
public class FileBucket {
	MultipartFile file;
	
	String description;

	/**
	 * @return MultipartFile type
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file sets the FileBucket that is a MultipartFile type
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * @return description of file that is set by the uploader
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description sets the file's description set by uploader
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}