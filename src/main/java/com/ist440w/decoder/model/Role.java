package com.ist440w.decoder.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Team 1
 *
 */
@Entity
@Table(name = "role")
public class Role {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="role_id")
	private int id;
	@Column(name="role")
	private String role;
	
	/**
	 * @return id of the role
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id sets the role's identifier
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the role name
	 */
	public String getRole() {
		return role;
	}
	/**
	 * @param role sets the role's name
	 */
	public void setRole(String role) {
		this.role = role;
	}
	
	
}