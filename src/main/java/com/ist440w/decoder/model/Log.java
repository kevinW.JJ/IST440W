package com.ist440w.decoder.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Team 1
 *
 */
@Entity
@Table(name = "logs")
public class Log {
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id; 
    
    @Column(name="time_stamp", nullable=false)
    private Timestamp timeStamp;
    
    @Column(name="type", length=50, nullable=false)
    private String type;
    
    @Column(name="description", length=255, nullable=false)
    private String logMessage;
    
    /**
     * @return id of the log
     */
    public Integer getId() {
    	return id;
    }
    
    /**
     * @param id sets the Log's identifier
     */
    public void setId(Integer id) {
    	this.id = id;
    }
    
    /**
     * @return gets the type of log which can determine the severity
     */
    public String getType() {
    	return type;
    }
    
    /**
     * @param type of file type whether it is an image or document
     */
    public void setType(String type) {
    	this.type = type;
    }
    
    /**
     * @return timestamp of log
     */
    public Timestamp getTimeStamp() {
    	return timeStamp;
    }
    
    /**
     * @param timeStamp sets the time of log
     */
    public void setTimeStamp(Timestamp timeStamp) {
    	this.timeStamp = timeStamp;
    }
    
    /**
     * @return the message of the log
     */
    public String getLogMessage() {
    	return logMessage;
    }
    
    /**
     * @param logMessage sets the string logMessage
     */
    public void setLogMessage(String logMessage) {
    	this.logMessage = logMessage;
    }
    
    @Override
    public String toString() {
		return "Log #" + id + " ["+timeStamp.toString()+"]: " + logMessage;
    }

}
