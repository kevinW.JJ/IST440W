package com.ist440w.decoder.model;

import javax.persistence.*;

/**
 * @author Team 1
 *
 */
@Entity
@Table(name="user_document")
public class UserDocument {
 
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id; 
     
    @Column(name="name", length=100, nullable=false)
    private String name;
     
    @Column(name="description", length=255)
    private String description;
     
    @Column(name="type", length=100, nullable=false)
    private String type;
     
    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(name="content", nullable=false)
    private byte[] content;
 
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;
     
     
    /**
     * @return the id of the document or file upload
     */
    public Integer getId() {
        return id;
    }
 
    /**
     * @param id sets the id of the document/file
     */
    public void setId(Integer id) {
        this.id = id;
    }
 
    /**
     * @return the file name
     */
    public String getName() {
        return name;
    }
 
    /**
     * @param name sets the name of the document/file
     */
    public void setName(String name) {
        this.name = name;
    }
 
    /**
     * @return the description set by the uploader
     */
    public String getDescription() {
        return description;
    }
 
    /**
     * @param description sets the description by the uploader
     */
    public void setDescription(String description) {
        this.description = description;
    }
 
    /**
     * @return the type of file uploaded (e.g. image/png)
     */
    public String getType() {
        return type;
    }
 
    /**
     * @param type sets the type of document/file (e.g. image/png)
     */
    public void setType(String type) {
        this.type = type;
    }
 
    /**
     * @return content of file in byte[]
     */
    public byte[] getContent() {
        return content;
    }
 
    /**
     * @param content sets the content of file by byte[]
     */
    public void setContent(byte[] content) {
        this.content = content;
    }
 
    /**
     * @return the user who uploaded the file
     */
    public User getUser() {
        return user;
    }
 
    /**
     * @param user sets the user who uploads the file
     */
    public void setUser(User user) {
        this.user = user;
    }
 
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof UserDocument))
            return false;
        UserDocument other = (UserDocument) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return "UserDocument [id=" + id + ", name=" + name + ", description="
                + description + ", type=" + type + "]";
    }
 
 
     
}
